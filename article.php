<?php
require_once 'config.php';

$articleId;
$articleTitle;
$articleContent;
$articleDate;
$userName;

$articleId = $_GET['id'];

$query = "
SELECT article_title, article_content, article_date, users.user_name
FROM articles, users
WHERE article_id = '$articleId'
AND users.user_id = articles.user_id
";

$result = $db->query($query);
if ($db->errno)
	echo $db->error;
else
{
	$row = $result->fetch_assoc();
	$articleTitle = $row['article_title'];
	$articleContent = $row['article_content'];
	$userName = $row['user_name'];
}

$pageTitle = $articleTitle;
require_once 'head.php';
?>

<article>
	<header>
	<?php echo $articleTitle; ?>
	</header>
	
	<?php echo $articleContent; ?>
	
	<footer>
		<?php  
			echo $userName;
		?>
	</footer>
</article>

<?php
require_once 'foot.php';
?>